import http from 'http';
import assert from 'assert';

import '../src/index';

const options = {
  hostname: 'localhost',
  port: 4000,
  path: '/graphql',
  headers: {
    'Content-Type': 'application/json',
  },
};

describe('Example Node Server', () => {
  it('should return 200', (done) => {
    http.get(options, (res) => {
      assert.equal(200, res.statusCode);
      done();
    });
  });
});
