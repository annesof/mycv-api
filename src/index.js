
import server from './server';
// import connect from './db';

// connect();


server.listen(process.env.PORT || 4000).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
