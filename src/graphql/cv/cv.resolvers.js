
// import { Car } from '../../model';

const getCv = () => ({
  lastName: 'Evenot Deguelle',
  firstName: 'Anne-Sophie',
  title: 'Ingénieur développement fullstack',
  description: '',
  contact: {
    phoneNumber: '06 20 19 75 10',
    address: "1104 chemin d'Embalens",
    city: "Castelnau d'estretefonds",
    birtDate: 1980,
    license: 'Permis B',
    linkedIn: 'http://',
  },
  interests: [
    'Menuiserie', 'Hoop Dance', 'Voyage (Madagascar,VietNam, Etats-Unis...)',
  ],
  languages: [
    {
      name: 'Anglais',
      level: 'Bonne compréhension orale et écrite, rédaction technique' 
    },
  ],

});


export default{
  Query: {
    getCv,
  },
};
