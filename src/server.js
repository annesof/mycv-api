import { ApolloServer, gql } from 'apollo-server';
import fs from 'fs';
import merge from 'lodash.merge';
import cvResolvers from './graphql/cv/cv.resolvers';

const cv = fs.readFileSync('./src/graphql/cv/cv.graphql');


// Type definitions define the "shape" of your data and specify
// which ways the data can be fetched from the GraphQL server.
const typeDefs = gql`
  # Comments in GraphQL are defined with the hash (#) symbol.
    ${cv}
`;

// Resolvers define the technique for fetching the types in the
// schema.  We'll retrieve books from the "books" array above.
const resolvers = merge({}, cvResolvers);

// In the most basic sense, the ApolloServer can be started
// by passing type definitions (typeDefs) and the resolvers
// responsible for fetching the data for those types.
const server = new ApolloServer({
  typeDefs,
  resolvers,
  introspection: true,
  playground: true,
  tracing: true,
});

export default server;
